#ifndef RSINJARI_H
#define RSINJARI_H

#include "Crypto.h"

class rsinjari : public Crypto
{
    public:
        rsinjari();
        virtual ~rsinjari();

        std::string encode(std::string s);
        std::string decode(std::string s);

     protected:
    private:

};

#endif // RSINJARI_H
