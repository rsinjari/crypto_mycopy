#ifndef DISOTELL_H
#define DISOTELL_H

#include "Crypto.h"

class Disotell : public Crypto
{
    public:
        Disotell();
        virtual ~Disotell();
         std::string encode(std::string s);
         std::string decode(std::string s);
    protected:
    private:
};

#endif // DISOTELL_H
